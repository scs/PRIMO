from primo2.networks import BayesianNetwork
from primo2.nodes import RandomNode, DiscreteNode
import numpy as np
import networkx

def dseparation(bn, startNode, endNode, evidence):
    all_paths = []
    visited = []

    start_node = startNode
    goal_node = endNode
    evidence_nodes = []
    for key in evidence:
        evidence_nodes.append(key)

    def collider_has_evidence(visit):
        to_check = [visit]
        while len(to_check) > 0:
            check = to_check.pop(0)
            if check in evidence_nodes:
                return True
            for c in bn.get_children(bn.get_node(check)):
                to_check.append(c)
        return False

    def DFS(visit, goal_node, visited):
        visited.append(visit)
        if visit[0] == goal_node: # if we found our goal node this path is done
            all_paths.append(visited.copy())
        next_nodes = [] # store all reachable nodes from this node
        if visit[1] == "up":
            if visit[0] not in evidence_nodes:
                for c in bn.get_children(bn.get_node(visit[0])):
                    next_nodes.append((c.name, "down"))
                for p in bn.get_node(visit[0]).parentOrder:
                    next_nodes.append((p, "up"))
        if visit[1] == "down":
            if collider_has_evidence(visit[0]):
                for p in bn.get_node(visit[0]).parentOrder:
                    next_nodes.append((p, "up"))
            if visit[0] not in evidence_nodes:
                for c in bn.get_children(bn.get_node(visit[0])):
                    next_nodes.append((c.name, "down"))
        for n in next_nodes:
            if n[0] not in [i[0] for i in visited]:
                DFS(n, goal_node, visited)
        visited.remove(visit)

    DFS((start_node, "up"), goal_node, visited)


    return all_paths
