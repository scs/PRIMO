#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of PRIMO2 -- Probabilistic Inference Modules.
# Copyright (C) 2013-2017 Social Cognitive Systems Group, 
#                         Faculty of Technology, Bielefeld University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the Lesser GNU General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public 
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

from operator import itemgetter

from ..networks import BayesianNetwork

def create_interaction_graph(graph):
    res = graph.to_undirected()
    for node in graph.nodes():
        for p1 in graph[node]:
            for p2 in graph[node]:
                if p1 != p2 and p1 not in graph[p2]:
                    res.add_edge(p1,p2)
                    # res.add_edge(p2,p1)
    return res

class Orderer(object):
    """
        A "static" class that provides the functionality to create elimination orders
        from Bayesian networks.
    """
    
    @staticmethod
    def get_min_degree_order(bn):
        """
            Returns the elimination order according to the min degree order algorithm
            explained in "Modeling and Reasoning with Bayesian Networks" - Adnan Darwiche
            Chapter 6.6
            
            Parameter
            ---------
            bn : BayesianNetwork
                The network for which the order is to be determined.
                
            Returns
            -------
                [String,]
                A list of variable names, representing the elimination order of 
                these variables.
        """
        if not isinstance(bn, BayesianNetwork):
            raise TypeError("Only Bayesian Networks are currently supported.")
        
        interactionG = create_interaction_graph(bn.graph) #bn.graph.to_undirected()
        res = []        
        for i in range(len(bn.get_all_node_names())):
            degrees = dict(interactionG.degree())
            varToElim = sorted(degrees.items(), key=itemgetter(1))[0][0]
            res.append(varToElim.name)
            for p in interactionG[varToElim]:#.parents:
                for p2 in interactionG[varToElim]:#varToElim.parents:
                    if p != p2 and not p2 in interactionG[p]:#:
#                        interactionG.add_edge(varToElim.parents[p], varToElim.parents[p2])
                        interactionG.add_edge(p,p2)
            interactionG.remove_node(varToElim)
        return res

    @staticmethod
    def get_random_order(bn):
        """
            Returns the elimination order according to the order in which the nodes
            are stored internally. This is deterministic but as dictionaries
            are used to store the nodes, this might no be intuitive.
            
            Parameter
            ---------
            bn : BayesianNetwork
                The network for which the order is to be determined.
                
            Returns
            -------
                [String,]
                A list of variable names, representing the elimination order of 
                these variables.
        """                
        if not isinstance(bn, BayesianNetwork):
            raise TypeError("Only Bayesian Networks are currently supported.")
            
        return bn.get_all_node_names()
        
    