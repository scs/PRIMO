#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of PRIMO2 -- Probabilistic Inference Modules.
# Copyright (C) 2013-2017 Social Cognitive Systems Group, 
#                         Faculty of Technology, Bielefeld University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the Lesser GNU General Public License as 
# published by the Free Software Foundation, either version 3 of the 
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public 
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

import random
import numpy as np

from .factor import Factor

class MCMC(object):
    
    def __init__(self, bn, transitionModel=None, numSamples=1000, burnIn=1000, fullChange=False):
        """
            Creates a markov cain monte carlo instance which is used to 
            approximate marginals on a given BayesianNetwork.
            
            Parameters
            ----------
            bn : BayesianNetwork
                The network that is used to approximate the marginals
                
            transitionModel : TransitionModel, optional
                The transition model that should be used to sample the next
                state. (Default: None -> Uses Metropolis Hastings)
                
            numSamples : int, optional
                Number of samples used to approximate the probabilities. (Default: 1000)
                
            burnIn : int, optional
                The number of samples that should be discarded as burnIn. (Default: 1000)
                
            fullChange : bool, optional
                If true, a step in the sampler is considered after resampling all variables, 
                instead of only a single one (Default: False)
        """
        self.bn = bn
        self.numSamples = numSamples
        self.sampler = MarkovChainSampler(transitionModel, burnIn, fullChange)
    
    def marginals(self, variables, evidence=None, sampleChain=None, stepByStep=False, sampledVars=None):
        """
            Function that approximates the joint prior or posterior marginals for the
            given variables, potentially given some evidence.
            
            Parameters
            ---------
            variables : [String,]
                List containing the names of the desired variables.
                
            evidence : dict, optional
                Dictionary containing the evidence variables as keys and their
                instantiations as values.

            sampleChain : Generator of Samples (produced by MarkovChainSampler.generate_markov_chain)
                An optional sample chain that already exists.
                
            Returns
            -------
                Factor
                A factor over the given variables representing their joint probability 
                given the evidence.
        """
        if not evidence:
            evidence = {}
        if sampleChain is None:
            initialState = self.bn.get_sample(evidence)
            sampleChain = self.sampler.generate_markov_chain(self.bn, self.numSamples, initialState, evidence, stepByStep)
        # Compute probability for variables given the samples
        if stepByStep:
            res_dict = {}
            for v in variables:
                res_dict[v] = np.zeros([len(self.bn.get_node(v).values), self.numSamples, 4])
            for i, s in enumerate(sampleChain):
                for v in res_dict:
                  res_dict[v][self.bn.get_node(v).values.index(s[v])][i][0] = 1
                  if v == sampledVars[i]:
                      res_dict[v][0][i][3] = 1
                  for p in range(len(res_dict[v])):
                    curSum = np.sum(res_dict[v][p][:,0])
                    res_dict[v][p][i][1] = curSum
                    res_dict[v][p][i][2] = curSum/(i+1)
            return res_dict
        else:
            variableValues = {v: self.bn.get_node(v).values for v in variables}
            return Factor.from_samples(sampleChain, variableValues)
        
        
    
    
class MarkovChainSampler(object):
    
    def __init__(self, transitionModel=None, burnIn=1000, fullChange = False):
        """
            Creates a markov chain sampler that creates samples given
            a network and a transition model.
            
            Parameters
            ----------
            transitionModel : TransitionModel, optional
                The model used to transition from the current state to the next state.
                If not specified, MetropolisHastingsTransition is used.
                
            burnIn : int
                Number of samples to discard before actually collecting and returning
                samples. (Default 1000)
                
            fullChange : bool, optional
                If true, a step is considered after resampling all variables, instead of
                only a single one (Default: False)
        """
        self.burnIn = burnIn
        self.fullChange = fullChange
        if not transitionModel:
#            transitionModel = GibbsTransition()
            transitionModel = MetropolisHastingsTransition()
        self.transitionModel = transitionModel
    
    def generate_markov_chain(self, bn, numSamples, initialState, evidence=None, stepByStep=False, sampledVars=None):
        """
            Generator actually yielding the given number of samples drawn from 
            the given network starting from the initialState.
            
            Parameters
            ----------
            network : BayesianNetwork
                The network from which the samples are drawn.
                
            numSamples : int
                The number of samples this generator returns in total
                
            initialState : dict
                A dictionary containing RandomNodes as keys and their instantiation
                for the initial state of the network as values.
                
            evidence : dict, optional
                A dictionary containg the evidence variable as keys and their
                instantiation as values.
            
            Yields
            -------
                dict
                A dictionary containing the RandomNodes as keys and their current
                instantiation as values.
        """
        if evidence is None:
            evidence = {}
        variablesToChange = [node for node in bn.get_all_nodes() if node not in evidence]
        curSamples = 0
        # if stepByStep:
        #   state = [dict(initialState), '']
        # else:
        state = initialState
        while curSamples < self.burnIn:
            state = self.transitionModel.step(dict(state), variablesToChange, bn, self.fullChange, stepByStep)
            if stepByStep:
                state = state[0]
            curSamples += 1
        for i in range(numSamples):
            state = self.transitionModel.step(dict(state), variablesToChange, bn, self.fullChange, stepByStep)
            if stepByStep:
                if sampledVars != None:
                    sampledVars.append(state[1])
                state = state[0]
            yield state
    
class TransitionModel(object):
    """
        Abstract class defining a transition model.
    """
    
    def step(self, currentState, variables, bn, fullChange=False, stepByStep=False):
        raise NotImplementedError("Should be overwritten by inheriting class.")
    
class GibbsTransition(TransitionModel):

    def __init__(self):
        self.checked_vars = set()
        self.cur_vars_to_change = list()
    
    def step(self, currentState, variablesToChange, bn, fullChange=False, stepByStep=False):
        """
            A method that performs "one" markov step according to Gibbs sampling.
            This implementation will make sure that all variables are sampled once before
            a variable can be resampled a second time.
            (See "Probabilistic Graphical Models, Daphne Koller and Nir Friedman" (p.506))
            
            Parameters
            ----------
            currentState : dict
                Dictionary containing the variables as keys and their current instantiation
                as values.
            
            variablesToChange : list
                List containing the variables for which a new state needs to be sampled.
                
            bn : BayesianNetwork
                The network that is supposed to be samples.
            
            fullChange: Boolean, optional
                If True, will create a new sample by sampling all non-evidence
                variables again. Default: False
        """
        if fullChange:
            for v in variablesToChange:
                currentState = dict(currentState)
                currentState[v.name] = v.sample_value(currentState, bn.get_children(v.name))
        else:
            if set(variablesToChange) != set(self.cur_vars_to_change):
                self.checked_vars = set()
                self.cur_vars_to_change = variablesToChange

            remaining_vars = list(set(variablesToChange)-self.checked_vars)
            if len(remaining_vars) == 0:
                self.checked_vars = set()
                remaining_vars = list(variablesToChange)
            if remaining_vars:
                varToChange = random.choice(remaining_vars)
                self.checked_vars.add(varToChange)
                currentState[varToChange.name] = varToChange.sample_value(currentState, bn.get_children(varToChange.name))
                if stepByStep:
                    return currentState, varToChange

        return currentState

    
class MetropolisHastingsTransition(TransitionModel):
    
    def step(self, currentState, variablesToChange, bn, fullChange = False, stepByStep=False):
        """
            A method that performs "one" markov step according to MetropolisHasting sampling.
            (See "Probabilistic Graphical Models, Daphne Koller and Nir Friedman" (p.516))
            
            Parameters
            ----------
            currentState : dict
                Dictionary containing the variables as keys and their current instantiation
                as values.
            
            variablesToChange : list
                List containing the variables for which a new state needs to be sampled.
                
            bn : BayesianNetwork
                The network that is supposed to be samples.
            
            fullChange: Boolean, optional
                If True, will create a new sample by sampling all non-evidence
                variables again. Default: False
        """
        if fullChange:
            for v in variablesToChange:
                proposedValue = v.sample_local(currentState[v.name])
                adoptedState = dict(currentState)
                adoptedState[v.name] = proposedValue
                proposedProb = v.get_markov_prob(proposedValue, bn.get_children(v.name), adoptedState)
                currentProb = v.get_markov_prob(currentState[v.name], bn.get_children(v.name), currentState)
                
                accept = min(1.0, proposedProb/currentProb)
                if random.random() <= accept:
                    currentState[v.name] = proposedValue
        else:
            if variablesToChange:
                varToChange = random.choice(variablesToChange)
                proposedValue = varToChange.sample_local(currentState[varToChange])
                adoptedState = dict(currentState)
                adoptedState[varToChange.name] = proposedValue
                proposedProb = varToChange.get_markov_prob(proposedValue, bn.get_children(varToChange), adoptedState)
                currentProb = varToChange.get_markov_prob(currentState[varToChange], bn.get_children(varToChange), currentState)
                
                accept = min(1.0, proposedProb/currentProb)
                
                if random.random() <= accept:
                    currentState[varToChange.name] = proposedValue
            
        return currentState
            