# PRIMO - PRobabilistic Inference MOdules

This project is a (partial) reimplementation of the original probabilistic inference modules (see branch primo-legacy). This reimplementation follows the same general idea, but restructured and unified the underlying datatypes to allow a more concise API and more efficient manipulation, e.g., by the inference algorithm. In turn the inference algorithms have been rewritten and partly extended. For most if not all use cases this implementation should be easier to use and more performant than the original.


## Setup/Installation

In order to install PRIMO, you can just use the provided `setup.py`:

`python setup.py install`

## Basic usage

There are different ways to use PRIMO. We recommend you check the `examples` or tests under `primo2/tests` first to see different usecases.

## Contents

* (Dynamic) Bayesian Networks
* Rudimentary implementation of Decision Networks
* Exact inference using Variable Elimination and JunctionTree
* Approximate Inference using Metropolis Hastings